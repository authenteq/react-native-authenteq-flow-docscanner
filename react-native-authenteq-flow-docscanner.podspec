require "json"

package = JSON.parse(File.read(File.join(__dir__, "package.json")))

Pod::Spec.new do |s|
  s.name         = "react-native-authenteq-flow-docscanner"
  s.version      = package["version"]
  s.summary      = package["description"]
  s.description  = <<-DESC
                   React Native module for Authenteq Flow DocScanner
                   DESC
  s.homepage     = "https://bitbucket.org/authenteq/react-native-authenteq-flow-docscanner.git"
  s.license      = "commercial"
  s.authors      = { "Authenteq" => "info@authenteq.com" }
  s.platforms    = { :ios => "11.0" }
  s.source       = { :git => "https://bitbucket.org/authenteq/react-native-authenteq-flow-docscanner.git" }

  s.swift_version = "5"
  s.requires_arc = true

  s.dependency "React"
  s.dependency "AuthenteqFlowDocScanner", "1.76.0"
end
